#!/bin/bash

mkdir /home/bitnami/elabbackup/backups;

/home/bitnami/stack/mysql/bin/mysqldump -u elabuser -pelabuserdbpassword ELAB_DB  > /home/bitnami/elabbackup/"backups/elab_db_`date '+%d-%m-%y-%H.%M.%S'`.sql";

